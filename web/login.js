/**
 * Checks if a mail address is valid.
 */
function isValidUsername(show) {
    // take the username introduced in text box
    var username = document.forms["authenticationForm"]["username"].value;

    if (username == "") {
        if (show) {
            invalidElement("usernameTextId", "errorMessageUsernameId",
                "You didn't entered a username.<br>");
        }
        return false;
    }

    if ((username.length < 3) || (username.length > 15)) {
        if (show) {
            invalidElement("usernameTextId", "errorMessageUsernameId",
                "The username is the wrong length.<br>");
        }
        return false;
    }

    return true;
}

/**
 * Checks if a password is valid.
 */
function isValidPassword(show) {

    // take the password introduced in text box
    var password = document.forms["authenticationForm"]["password"].value;

    if (password == "") {
        if (show) {
            invalidElement("passwordTextId", "errorMessagePasswordId",
                "You didn't enter a password.<br>");
        }
        return false;
    }

    if ((password.length <= 3) || (password.length > 15)) {
        if (show) {
            invalidElement("passwordTextId", "errorMessagePasswordId",
                "The password is the wrong length.<br>");
        }
        return false;
    }

    /*
     * //var illegalChars = /[\W_]/; // allow only letters and numbers
     *
     * if (illegalChars.test(password)) { error = "The password contains illegal
     * characters.\n"; isValid = false; } else if (!((password.search(/(a-z)+/)) &&
     * (password.search(/(0-9)+/)))) { error = "The password must contain at
     * least one numeral.\n"; isValid = false; }
     */

    return true;
}

/**
 * Validates the username and the password and eventually enable the login
 * button.
 */
function isValidForm() {
    var isValidU = isValidUsername(true);
    var isValidP = isValidPassword(true);

    if (isValidU == false || isValidP == false) {
        document.getElementById("loginButtonId").disabled = true;
        return false;
    } else {
        document.getElementById("loginButtonId").disabled = null;
        return true;
    }
}

/**
 * Used if an element is not valid. It modifies the page.
 */
function invalidElement(elementId, textid, text) {
    if (text != null && text != "null") {
        document.getElementById(textid).innerHTML = text;
        document.getElementById(elementId).className = "redBackground";
    }
}

/**
 * Used if an element becomes valid (has focus). It modifies the page.
 */
function validateElement(elementId, textid) {
    document.getElementById(elementId).className = null;
    document.getElementById(textid).innerHTML = "<br>";
}

/**
 * If is valid the form, enables or disables the login button.
 */
function tryEnableLoginButton() {
    var isValidU = isValidUsername(false);
    var isValidP = isValidPassword(false);

    if (isValidU == false || isValidP == false) {
        document.getElementById("loginButtonId").disabled = true;
    } else {
        document.getElementById("loginButtonId").disabled = null;
    }
}


/********************************************************/


// declaring the AdRequester singleton class

var AdRequester = (function(){



    /*private variables and methods (not accessible directly through the  AdRequester namespace): */



    function performScreenSizeDetection(adURL){
    
        if (window.screen && window.screen.width && window.screen.height) {
        
            if (window.screen.height){
            
                adURL += ';kvscreenHeight=';
            
                adURL += window.screen.height;
            
            }
        
            if (window.screen.width){
            
                adURL += ';kvscreenWidth=';
            
                adURL += window.screen.width;
            
            }
        
            if (window.devicePixelRatio){
            
                adURL += ';kvscreenDensity=';
            
                adURL += window.devicePixelRatio;
            
            }
        
        }
    
        return adURL;
    
    }



    function buildAdURL(placementIdParam, kvmJSON){
    
        var adURL;
    
        if (window.Adtech['adURLHostName'+placementIdParam] &&
        
            window.Adtech['networkSubnetwork'+placementIdParam] &&
        
        window.Adtech['placementAlias'+placementIdParam]){
        
            adURL = "http://" + window.Adtech['adURLHostName'+placementIdParam]+
                
                    +"/addyn/3.0/"
        
            +window.Adtech['networkSubnetwork'+placementIdParam]
        
                    +"/0/0/-1/ADTECH;loc=100;alias="+window.Adtech['placementAlias'+placementIdParam]
        
                    +";misc="+new Date().getTime();
        
        }
    
        adURL = appendKVM(kvmJSON, adURL);
    
        return adURL;
    
    }



    function  appendKVM(kvmJSON, adURL){
    
        if (kvmJSON) {
        
            var jsonObject = JSON.parse(kvmJSON);
        
            for(var key in jsonObject){
            
                adURL +=";kv"+key+"=";
            
                if (Array.isArray){
                
                    if (Array.isArray(jsonObject[key])){
                    
                        for(var i in jsonObject[key]){
                        
                            if (i!=0){
                            
                                adURL +=",";
                            
                            }
                        
                            adURL +=jsonObject[key][i];
                        
                        }
                    
                    } else {
                    
                        adURL+=jsonObject[key];
                    
                    }
                
                }
            
            }
        
        }
    
        return adURL;
    
    }



    /* public variables and methods (can access private vars and methods ) */

    return {
    

            
        version : "1.0",
    

            
        requestAd: function(placementIdParam,kvmJSON){
        
            if (placementIdParam){
            
                var adURL = buildAdURL(placementIdParam, kvmJSON);
            
                if (adURL){
                
                    if (window.Adtech['performScreenDetection'+placementIdParam]) {
                    
                        adURL=performScreenSizeDetection(adURL);
                    
                    }
                
                    document.write('<scr'+'ipt src="'+adURL+'"></scri'+'pt>');
                
                }
            
            }
        
        }
    

            
    }

})();