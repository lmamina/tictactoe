<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="layout.css" type="text/css"/>
    <script type="text/javascript" src="login.js"></script>
</head>
<body background="${pageContext.request.contextPath}/images/background_image.png"/>
<jsp:useBean id="translationsBean" class="com.iquest.config.Translations" scope="application"/>
<jsp:useBean id="resourceBundleBean" class="com.iquest.config.LanguageMessages" scope="session"/>

<form name="languageForm" method="POST" action="${pageContext.request.contextPath}/changeLanguage">
    <select name="language" onchange="this.form.submit()">
        <c:forEach items="${translationsBean.supportedLanguages}" var="locale">

            <c:choose>
                <c:when test="${locale.language eq resourceBundleBean.locale}">
                    <option value="${locale.language}" selected>${locale.displayLanguage}</option>
                </c:when>
                <c:otherwise>
                    <option value="${locale.language}">${locale.displayLanguage}</option>
                </c:otherwise>
            </c:choose>

        </c:forEach>
    </select>
</form>
<form name="authenticationForm" method="POST" action="${pageContext.request.contextPath}/login"
      onsubmit="return isValidForm()" id="authenticationForm">


    <div>
        <script language='javascript'>AdRequester.requestAd('1','{"anotherTest":[1, "XML"],"test":"value"}');</script>
    </div>

    <%--show a message when authentication failed--%>
    <c:if test="${loginFailed eq true}">
        <p class="redText">Authentication failed!</p>
        <%request.getSession().setAttribute("loginFailed", null);%>
    </c:if>

    <%--show a message when user logged out--%>
    <c:if test="${logout eq true}">
        You are logged out now.
        <%request.getSession().setAttribute("logout", null);%>
    </c:if>

    <p id="errorMessageUsernameId" class="redText"><br></p>

    <p id="errorMessagePasswordId" class="redText"><br></p>

    <p class="authenticationText">
        <c:out value="${resourceBundleBean.getMessage('authenticationLabel')}:"/>
    </p><br>

    <c:out value="${resourceBundleBean.getMessage('usernameLabel')}:"/>
    <input id="usernameTextId" type="text" name="username"
           onFocus="validateElement('usernameTextId', 'errorMessageUsernameId')"
           onblur="isValidForm()" onkeyup="tryEnableLoginButton()"> <br>

    <c:out value="${resourceBundleBean.getMessage('passwordLabel')}:"/>
    <input id="passwordTextId" type="password" name="password"
           onFocus="validateElement('passwordTextId', 'errorMessagePasswordId')"
           onblur="isValidForm()" onkeyup="tryEnableLoginButton()"> <br>

    <br>
    <input class="loginButton" id="loginButtonId" type="submit" name="Login"
           value="${resourceBundleBean.getMessage('loginButton')}"
           disabled="disabled">

</form>
</body>
</html>