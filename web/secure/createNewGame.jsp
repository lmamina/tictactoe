<html>
<head>
    <link rel="stylesheet" href="../layout.css" type="text/css">
    <script type="text/javascript" src="createNewGame.js"></script>
</head>
<h4>Create New Game</h4>

<body background="${pageContext.request.contextPath}/images/background_image.png">
<form name="newGameForm" class="newGameForm" action="createNewGame" method="POST">

    <p id="errorMessageNameId"><br></p>

    <p id="errorMessageMatrixSizeId"><br></p>

    Name:<br><input type="text" name="name" id="nameTextId" onFocus="validateElement('nameTextId', 'errorMessageNameId')"
                    onblur="isValidForm()"
                    onkeyup="tryEnableCreateGameButton()"><br>
    Size:<br><input type="text" name="matrixSize" id="matrixSizeTextId" value="3" onFocus="validateElement('matrixSizeTextId', 'errorMessageMatrixSizeId')"
                    onblur="isValidForm()"
                    onkeyup="tryEnableCreateGameButton()"><br>
    Your character option:
    <select name="option">
        <option>X</option>
        <option>O</option>
    </select>
    <br><br>
    <input type="submit" id="createNewGameButtonId" class="createNewGameButton" name="createNewGameButton" value="Create Game" disabled="disabled">
</form>
</body>
</html>