/**
 * Validates the name and the size and eventually enable the create game button.
 */
function isValidForm() {	
	var isValidN = isValidName(true);
	var isValidS = isValidSize(true);

	if (isValidN == false || isValidS == false) {
		document.getElementById("createNewGameButtonId").disabled = true;
		return false;
	} else {
		document.getElementById("createNewGameButtonId").disabled = null;
		return true;
	}
}


/**
 * Checks if the name of the game is valid.
 */
function isValidName(show) {

	// take the username introduced in text box
	var username = document.forms["newGameForm"]["name"].value;

	if (username == "") {
		if (show) {
			invalidElement("nameTextId", "errorMessageNameId",
					"You didn't entered a name.<br>");
		}
		return false;
	}

	if ((username.length <= 3) || (username.length > 15)) {
		if (show) {
			invalidElement("nameTextId", "errorMessageNameId",
					"The name is the wrong length. It has to be between 4-15 characters.<br>");
		}
		return false;
	}

	return true;
}

/**
 * Checks if the game size is valid.
 * @param show
 * @returns {Boolean}
 */
function isValidSize(show) {
	// take the size introduced in text box
	var size = document.forms["newGameForm"]["matrixSize"].value;

	if (size == "") {
		if (show) {
			invalidElement("matrixSizeTextId", "errorMessageMatrixSizeId",
					"You didn't entered a size.<br>");
		}
		return false;
	}

	if (size.length > 2) {
		if (show) {
			invalidElement("matrixSizeTextId", "errorMessageMatrixSizeId",
					"The size is too big.<br>");
		}
		return false;
	}

	return true;
}

/**
 * If is valid the form, enables or disables the login button
 */
function tryEnableCreateGameButton() {
	var isValidN = isValidName(false);
	var isValidS = isValidSize(false);

	if (isValidN == false || isValidS == false) {
		document.getElementById("createNewGameButtonId").disabled = true;
	} else {
		document.getElementById("createNewGameButtonId").disabled = null;
	}
}

/**
 * Used if an element is not valid. It modifies the page.
 */
function invalidElement(elementId, textid, text) {
	if (text != null && text != "null") {
		document.getElementById(textid).innerHTML = text;
		document.getElementById(elementId).className = "redBackground";
	}
}

/**
 * Used if an element becomes valid (has focus). It modifies the page.
 */
function validateElement(elementId, textid) {
	document.getElementById(elementId).className = null;
	document.getElementById(textid).innerHTML = "<br>";
}