<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${gameEnded == true}">
        <script type="text/javascript">unload();</script>
        <c:choose>
            <c:when test="${winner eq player}">
                <p class="gameWinner">You win!</p>
            </c:when>
            <c:otherwise>
                <p class="gameFailure">Game Over!</p>
            </c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/secure/games.jsp" class="fontSize27">Games list</a>
    </c:when>
    <c:otherwise>
        <p class="gamePlayer">Waiting for <%=request.getAttribute("userShift")%>...</p>
    </c:otherwise>
</c:choose>

<table title="Game" id="tableId" border="1">
    <c:forEach items="${matrix}" var="row">
        <tr>
            <c:forEach items="${row}" var="cell">
                <td height="100" width="100">${cell}</td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
