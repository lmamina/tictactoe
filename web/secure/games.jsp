<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layout.css" type="text/css">
    <script
        src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"
        type="text/javascript">
    </script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/secure/games.js"></script>
</head>

<body id="body" onload="load()" background="${pageContext.request.contextPath}/images/background_image.png">
<form method="GET" action="${pageContext.request.contextPath}/secure/createNewGame.jsp">
    <input class="newGameButton" type="submit" name="newGameButton" value="New Game">
</form>

<h4>Existing Games:</h4>

<div id="existingGamesTable">
    <%@ include file="existingGames.jsp" %>
</div>
</body>
</html>