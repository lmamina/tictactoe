<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.*"%>
<head>
<link rel="stylesheet" href="../layout.css" type="text/css">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"
	type="text/javascript"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/secure/play.js"></script>
</head>
<html>

<body id="body" onload="load()" background="${pageContext.request.contextPath}/images/background_simple.png">
	<% String game = request.getParameter("gameName"); %>
	<input type="hidden" id="gameName" value="<%=game%>">
	<div id="gameTable">
		<table id="tableId" title="Game" border="1"/>
	</div>
</body>
</html>