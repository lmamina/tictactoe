var idInterval;

function load() {
    setTableOnClick();
    refreshTable();
    idInterval = window.setInterval(refreshTable, 1000);
}

function setTableOnClick() {
    var myTblCells = document.getElementById("tableId").getElementsByTagName("td");
    for (var t = 0, curCell; curCell = myTblCells[t]; t++) {
        curCell.onclick = mark;
    }
}

function mark() {
    var url = "executeShift?gameName=" + document.getElementById("gameName").value + "&row=" + this.parentNode.rowIndex + "&column=" + this.cellIndex;
    $('#gameTable').load(url, function () {
        setTableOnClick();
    });
}

function refreshTable() {
    $('#gameTable').load("refreshTable?gameName=" + document.getElementById("gameName").value, function () {
        setTableOnClick();
    });
}

function unload() {
    window.clearInterval(idInterval);
}