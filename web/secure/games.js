/**
 * Sets the window to refresh every second.
 */
function load() {
	window.setInterval(refresh, 1000);
}

/**
 * Refreshes the page.
 */
function refresh() {
	$('#existingGamesTable').load("../secure/existingGames.jsp");
}