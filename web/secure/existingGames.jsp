<%@ page import="com.iquest.model.GameStatus" %>
<%@ page import="com.iquest.repository.GameRepository" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("gameRepoClassName", GameRepository.class.getName());%>

<table title="Existing Games" border="1">
    <tr>
        <td>Game</td>
        <td>Status</td>
    </tr>

    <c:forEach var="game" items="${applicationScope[gameRepoClassName][\"games\"]}">
        <tr>
            <td>
                ${game.name}
            </td>
            <td>
                <c:choose>
                    <c:when test="${game.gameStatus eq 'STATUS_WAITING_FOR_PLAYER'}">
                        <a href="playOptions?gameName=${game.name}">${game.gameStatus.getStatus()}</a>
                    </c:when>
                    <c:otherwise>
                        ${game.gameStatus.getStatus()}
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>

</table>