package com.iquest.parser;

import com.iquest.repository.file.PlayerFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author Laura Mamina
 */
public class PlayersParser {

  private static final Logger LOGGER = LoggerFactory.getLogger(PlayersParser.class);

  public PlayersParser() {
  }

  public PlayerFileRepository parse(String fileName) throws JAXBException, FileNotFoundException {
    LOGGER.info("Parsing " + fileName + " to populate the player repository");

    // create JAXB context and initializing Unmarshaller
    JAXBContext jaxbContext = JAXBContext.newInstance(PlayerFileRepository.class);
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

    // specify the location and name of xml file to be read
    InputStream XMLfile = new FileInputStream(new File(fileName));

    // this will create Java objects from the XML file
    return (PlayerFileRepository) jaxbUnmarshaller.unmarshal(XMLfile);
  }
}
