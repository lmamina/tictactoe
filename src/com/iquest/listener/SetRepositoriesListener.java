package com.iquest.listener;

import com.iquest.exceptions.RetrieveException;
import com.iquest.repository.GameRepository;
import com.iquest.repository.PlayerRepository;
import com.iquest.repository.db.utils.DBUtils;
import com.iquest.repository.file.GameFileRepository;
import com.iquest.retriver.PlayersRetriever;
import com.iquest.retriver.PlayersRetrieverFromDB;
import com.iquest.retriver.PlayersRetrieverFromFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Attach the repositories to the context.
 *
 * @author: Laura Mamina
 */
public class SetRepositoriesListener implements ServletContextListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(SetRepositoriesListener.class);

  private PlayersRetriever playersRetriever;
  private boolean dbConfigured;

  @Override
  public void contextDestroyed(ServletContextEvent servletContext) {
  }

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    String dataSourceName = null;//servletContextEvent.getServletContext().getInitParameter("dataSourceName");
    String playersFile = servletContextEvent.getServletContext().getInitParameter("playersFile");

    resolveRepositoryConfiguration(dataSourceName, playersFile);
    playersRetriever = resolvePlayersRetriever(dataSourceName, playersFile, servletContextEvent);

    PlayerRepository playerRepository = playersRetriever.getRepository();
    servletContextEvent.getServletContext().setAttribute(PlayerRepository.class.getName(), playerRepository);

    GameRepository gameRepository = resolveGamesRepository();
    servletContextEvent.getServletContext().setAttribute(GameRepository.class.getName(), gameRepository);

    LOGGER.debug("Context initialized");
  }

  private void resolveRepositoryConfiguration(String dataSourceName, String playersFile) {
    if (dataSourceName == null && playersFile == null) {
      LOGGER.error("None of datasource or the file are set for getting the players.");
      throw new RetrieveException("None of datasource or the file are set for getting the players.");
    } else if (dataSourceName != null && playersFile != null) {
      LOGGER.info("Both datasource and the file are set for getting the players. The datasource will be used.");
    }

    if (dataSourceName != null) {
      dbConfigured = true;
      DBUtils.setDataSourceName(dataSourceName);
      LOGGER.debug("Data source name is: " + dataSourceName);
    } else if (playersFile != null) {
      dbConfigured = false;
      LOGGER.debug("Players file is: " + playersFile);
    }
  }

  private PlayersRetriever resolvePlayersRetriever(String dataSourceName, String playersFile, ServletContextEvent servletContextEvent) {
    //Instantiate the players retriever mode
    if (dbConfigured) {
      playersRetriever = new PlayersRetrieverFromDB();
    } else {
      ServletContext context = servletContextEvent.getServletContext();
      String playersFileRealPath = context.getRealPath(playersFile);
      LOGGER.debug("File to use is: " + playersFileRealPath);
      playersRetriever = new PlayersRetrieverFromFile(playersFileRealPath);
    }

    return playersRetriever;
  }

  private GameRepository resolveGamesRepository() {
    //if (dbConfigured) {
    //return new GameDBRepository();
    //}
    //else {
    return new GameFileRepository();
    //}
  }
}
