package com.iquest.listener;

import com.iquest.config.Translations;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import static com.iquest.config.ApplicationConfig.*;

/**
 * @author Laura Mamina
 */
public class ApplicationInitializer implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    ServletContext ctx = servletContextEvent.getServletContext();
    String appPropertiesFileName = ctx.getRealPath(ctx.getInitParameter(APP_PROPERTIES_CONFIG_PARAM.value()));

    Properties properties = loadProperties(appPropertiesFileName);
    Translations translations = loadTranslations(properties);

    ctx.setAttribute(TRANSLATIONS_BEAN.value(), translations);
  }

  private Properties loadProperties(String appPropertiesFileName) {
    Properties properties;
    try (InputStream propertiesInputStream = new FileInputStream(appPropertiesFileName)) {
      properties = new Properties();
      properties.load(propertiesInputStream);
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
    return properties;
  }

  private Translations loadTranslations(Properties properties) {
    String defaultLanguage = properties.getProperty(DEFAULT_LANGUAGE.value());
    String supportedLanguages = properties.getProperty(SUPPORTED_LANGUAGES.value());

    Translations translations = new Translations();
    translations.setDefaultLocale(new Locale(defaultLanguage));

    for (String language : supportedLanguages.split(VALUE_SEPARATOR.value())) {
      Locale locale = new Locale(language);
      translations.addBundle(locale, ResourceBundle.getBundle(BUNDLE_BASE_NAME.value(), locale));
    }
    return translations;
  }

  @Override
  public void contextDestroyed(ServletContextEvent servletContextEvent) {
  }
}