package com.iquest.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Tracks the requests.
 *
 * @author: Laura Mamina
 */
public class TrackSessionListener implements HttpSessionListener {

  private Logger logger = LoggerFactory.getLogger(TrackSessionListener.class);

  @Override
  public void sessionCreated(HttpSessionEvent httpSessionEvent) {
    logger.trace("Session created!" + httpSessionEvent.getSession());
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
    logger.trace("Session destroyed!" + httpSessionEvent.getSession());
  }
}
