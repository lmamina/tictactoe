package com.iquest.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

/**
 * Tracks the requests.
 *
 * @author: Laura Mamina
 */
public class TrackRequestListener implements ServletRequestListener {

  private Logger logger = LoggerFactory.getLogger(TrackRequestListener.class);

  @Override
  public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
    logger.trace("Request destroyed!" + ((HttpServletRequest) servletRequestEvent.getServletRequest()).getRequestURI());
  }

  @Override
  public void requestInitialized(ServletRequestEvent servletRequestEvent) {
    logger.trace("Request initialized!" + ((HttpServletRequest) servletRequestEvent.getServletRequest()).getRequestURI());
  }
}
