package com.iquest.retriver;

import com.iquest.exceptions.RetrieveException;
import com.iquest.repository.PlayerRepository;

/**
 * @author Laura Mamina
 */
public interface PlayersRetriever {

  PlayerRepository getRepository() throws RetrieveException;

}
