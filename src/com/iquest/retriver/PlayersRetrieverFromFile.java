package com.iquest.retriver;

import com.iquest.exceptions.RetrieveException;
import com.iquest.parser.PlayersParser;
import com.iquest.repository.file.PlayerFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;

/**
 * @author Laura Mamina
 */
public class PlayersRetrieverFromFile implements PlayersRetriever {

  private static final Logger LOGGER = LoggerFactory.getLogger(PlayersRetrieverFromFile.class);
  private String playersFileRealPath;

  public PlayersRetrieverFromFile(String playersFileRealPath) {
	this.playersFileRealPath = playersFileRealPath;
  }

  @Override
  public PlayerFileRepository getRepository() throws RetrieveException {
	PlayersParser playersParser = new PlayersParser();
	try {
	  return playersParser.parse(playersFileRealPath);
	} catch (JAXBException | FileNotFoundException e) {
	  LOGGER.error("Could not get players from file " + playersFileRealPath);
	  throw new RetrieveException("Could not get players from file " + playersFileRealPath, e);
	}
  }
}
