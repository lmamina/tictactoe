package com.iquest.retriver;

import com.iquest.repository.db.PlayerDBRepository;
import com.iquest.repository.db.utils.DBUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Laura Mamina
 */
public class PlayersRetrieverFromDB implements PlayersRetriever {
  @Override
  public PlayerDBRepository getRepository() {
	PlayerDBRepository playerRepository = new PlayerDBRepository();
	return playerRepository;
  }
}
