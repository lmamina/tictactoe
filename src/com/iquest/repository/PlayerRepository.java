package com.iquest.repository;

import com.iquest.model.Player;

import java.util.Collection;

/**
 * @author Laura Mamina
 */
public interface PlayerRepository {
  public Player get(String username, String password);
}
