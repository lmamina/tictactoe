package com.iquest.repository.file;

import com.iquest.model.Player;
import com.iquest.repository.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Repository for players.
 *
 * @author Laura Mamina
 */
@XmlRootElement(name = "players")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class PlayerFileRepository implements PlayerRepository {

  private static final Logger LOGGER = LoggerFactory.getLogger(PlayerFileRepository.class);
  private Collection<Player> players;

  public PlayerFileRepository() {
    players = new LinkedList<>();
  }

  @XmlElement(name = "player", type = Player.class)
  public void setPlayers(Collection<Player> players) {
    this.players = players;
  }

  public Collection<Player> getPlayers() {
    return players;
  }

  public Player get(String username, String password) {
    for (Player player : players) {
      if (player.getUsername().equals(username) && player.getPassword().equals(password)) {
        LOGGER.debug("user: " + username + " and password: " + password + " was found!");
        return player;
      }
    }

    LOGGER.debug("user: " + username + " and password: " + password + " was not found!");
    return null;
  }

  @Override
  public String toString() {
    return players.toString();
  }
}
