package com.iquest.repository.file;

import com.iquest.model.Game;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Repository for games.
 *
 * @author Laura Mamina
 */
public class GameFileRepository implements GameRepository {

  private Logger logger = LoggerFactory.getLogger(GameFileRepository.class);
  private volatile Collection<Game> games;

  public GameFileRepository() {
    games = new LinkedList<>();
  }

  public Collection<Game> getGames() {
    return games;
  }

  public void add(Game game) {
    games.add(game);
  }

  public Game get(String name) {
    Iterator<Game> iterator = games.iterator();

    while (iterator.hasNext()) {
      Game game = iterator.next();

      if (game.getName().equals(name)) {
        logger.debug("Game: " + name + " was found!");
        return game;
      }
    }

    logger.debug("Game: " + name + " was not found!");
    return null;
  }

}
