package com.iquest.repository.db;

import com.iquest.exceptions.RetrieveException;
import com.iquest.model.Player;
import com.iquest.repository.PlayerRepository;
import com.iquest.repository.db.utils.DBUtils;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Laura Mamina
 */
public class PlayerDBRepository implements PlayerRepository {
  private static String SELECT_PLAYER = "select * from player where username = ? and password = ?";

  @Override
  public Player get(String username, String password) {
    Player player = null;

    try (Connection connection = DBUtils.getDataSource().getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(SELECT_PLAYER)) {
        statement.setString(1, username);
        statement.setString(2, password);

        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
          player = new Player();
          player.setUsername(username);
          player.setPassword(password);
        }

      } catch (SQLException e) {
        throw new RetrieveException("Error executing selecting player " + username + " " + password, e);
      }
    } catch (NamingException e) {
      throw new RetrieveException("Error retrieving datasource for location " + DBUtils.getDataSourceName(), e);
    } catch (SQLException e) {
      throw new RetrieveException("Error retrieving the connection for datasource " + DBUtils.getDataSourceName(), e);
    }

    return player;
  }


}