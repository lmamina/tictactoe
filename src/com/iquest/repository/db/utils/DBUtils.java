package com.iquest.repository.db.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author Laura Mamina
 */
public class DBUtils {

  private static String dataSourceName;

  public static void setDataSourceName(String dataSourceName) {
    DBUtils.dataSourceName = dataSourceName;
  }

  public static String getDataSourceName() {
    return dataSourceName;
  }

  public static DataSource getDataSource() throws NamingException {
    // Get a context for the JNDI look up
    Context ctx = new InitialContext();
    Context envContext = (Context) ctx.lookup("java:/comp/env");

    // Look up a data source
    DataSource ds = (DataSource) envContext.lookup(dataSourceName);

    return ds;
  }

}
