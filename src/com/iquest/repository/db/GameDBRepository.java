package com.iquest.repository.db;

import com.iquest.exceptions.RetrieveException;
import com.iquest.model.Game;
import com.iquest.repository.GameRepository;
import com.iquest.repository.db.utils.DBUtils;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

/**
 * @author Laura Mamina
 */
public class GameDBRepository implements GameRepository {
  private static String SELECT_ALL_GAMES = "select name, size from game";
  private static String INSERT_GAME = "insert into game (name, size) values (?, ?)";
  private static String SELECT_GAME = "select name, size from game where name = ?";

  @Override
  public Collection<Game> getGames() {
    Collection<Game> games = new LinkedList<>();

    try (Connection connection = DBUtils.getDataSource().getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(SELECT_ALL_GAMES)) {
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
          String gameName = resultSet.getString(1);
          int gameSize = resultSet.getInt(2);

          Game game = new Game(gameName, gameSize);
          games.add(game);
        }

      } catch (SQLException e) {
        throw new RetrieveException("Error executing selecting all games", e);
      }
    } catch (NamingException e) {
      throw new RetrieveException("Error retrieving datasource for location " + DBUtils.getDataSourceName(), e);
    } catch (SQLException e) {
      throw new RetrieveException("Error retrieving the connection for datasource " + DBUtils.getDataSourceName(), e);
    }

    return games;
  }

  @Override
  public void add(Game game) {
    try (Connection connection = DBUtils.getDataSource().getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(INSERT_GAME)) {
        statement.setString(1, game.getName());
        statement.setInt(2, game.getMatrix().length);
        statement.executeUpdate();
      } catch (SQLException e) {
        throw new RetrieveException("Error executing adding game " + game.getName(), e);
      }
    } catch (NamingException e) {
      throw new RetrieveException("Error retrieving datasource for location " + DBUtils.getDataSourceName(), e);
    } catch (SQLException e) {
      throw new RetrieveException("Error retrieving the connection for datasource " + DBUtils.getDataSourceName(), e);
    }
  }

  @Override
  public Game get(String name) {
    Game game = null;

    try (Connection connection = DBUtils.getDataSource().getConnection()) {
      try (PreparedStatement statement = connection.prepareStatement(SELECT_GAME)) {
        statement.setString(1, name);
        statement.executeQuery();

        ResultSet resultSet = statement.getResultSet();

        if (resultSet.next()) {
          String gameName = resultSet.getString(1);
          int gameSize = resultSet.getInt(2);

          game = new Game(gameName, gameSize);
        }
      } catch (SQLException e) {
        throw new RetrieveException("Error executing getting game " + name, e);
      }
    } catch (NamingException e) {
      throw new RetrieveException("Error retrieving datasource for location " + DBUtils.getDataSourceName(), e);
    } catch (SQLException e) {
      throw new RetrieveException("Error retrieving the connection for datasource " + DBUtils.getDataSourceName(), e);
    }

    return game;
  }

}
