package com.iquest.repository;

import com.iquest.model.Game;

import java.util.Collection;

/**
 * @author Laura Mamina
 */
public interface GameRepository {

  public Collection<Game> getGames();

  public void add(Game game);

  public Game get(String name);

}
