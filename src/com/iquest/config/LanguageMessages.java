package com.iquest.config;

import java.util.ResourceBundle;

/**
 * @author Laura Mamina
 */
public class LanguageMessages {

  private ResourceBundle bundle;

  public LanguageMessages() {
  }

  public void setBundle(ResourceBundle bundle) {
    this.bundle = bundle;
  }

  public String getMessage(String message) {
    return bundle.getString(message);
  }

  public String getLocale() {
    return bundle.getLocale().getLanguage();
  }
}
