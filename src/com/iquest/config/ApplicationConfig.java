package com.iquest.config;

/**
 * @author Laura Mamina
 */
public enum ApplicationConfig {

  SUPPORTED_LANGUAGES("supportedLanguages"),
  DEFAULT_LANGUAGE("defaultLanguage"),
  VALUE_SEPARATOR(","),
  BUNDLE_BASE_NAME("MessagesBundle"),
  APP_PROPERTIES_CONFIG_PARAM("appPropertiesFile"),
  TRANSLATIONS_BEAN("translationsBean"),
  RESOURCE_BUNDLE_BEAN("resourceBundleBean");

  private String propertyName;

  private ApplicationConfig(String propertyName) {
    this.propertyName = propertyName;
  }

  public String value() {
    return propertyName;
  }
}
