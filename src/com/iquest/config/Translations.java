package com.iquest.config;

import java.util.*;

/**
 * @author Laura Mamina
 */
public class Translations {
  private Locale defaultLocale;
  private Map<Locale, ResourceBundle> translations;

  public Translations() {
    translations = new HashMap<>();
  }

  public Locale getDefaultLocale() {
    return defaultLocale;
  }

  public void setDefaultLocale(Locale defaultLocale) {
    this.defaultLocale = defaultLocale;
  }

  public ResourceBundle getBundle(Locale locale) {
    return translations.get(locale);
  }

  public void addBundle(Locale locale, ResourceBundle bundle) {
    translations.put(locale, bundle);
  }

  public Set<Locale> getSupportedLanguages() {
    return translations.keySet();
  }
}
