package com.iquest.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Sets the pageEncoding.
 *
 * @author Laura Mamina
 */
public class EncodingFilter implements Filter {

  private static final String ENCODING_PARAM = "encoding";
  private String pageEncoding;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    pageEncoding = filterConfig.getInitParameter(ENCODING_PARAM);
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    if (pageEncoding != null) {
      servletRequest.setCharacterEncoding(pageEncoding);
      servletResponse.setCharacterEncoding(pageEncoding);
    }
    filterChain.doFilter(servletRequest, servletResponse);
  }

  @Override
  public void destroy() {
  }
}
