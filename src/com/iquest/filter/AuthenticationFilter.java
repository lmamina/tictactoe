package com.iquest.filter;

import com.iquest.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter for authentication. The user gets the requested page just if he is authenticated, otherwise he is redirected to a page which informs him that he has no access.
 *
 * @author Laura Mamina
 */
public class AuthenticationFilter implements Filter {

  private Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

  @Override
  public void init(FilterConfig arg0) throws ServletException {
  }

  @Override
  public void destroy() {
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    HttpServletRequest httpReq = (HttpServletRequest) servletRequest;

    Player player = (Player) httpReq.getSession().getAttribute("player");

    if (player != null) {
      logger.debug("Authentication filter is running for request: " + httpReq.getRequestURI() + ". Player: " + player + " is logged in.");
      filterChain.doFilter(servletRequest, servletResponse);
    }
    else {
      logger.debug("Authentication filter is running for request: " + httpReq.getRequestURI() + ". No player is logged in.");
      ((HttpServletResponse) servletResponse).sendRedirect(httpReq.getContextPath() + "/no_access.html");
    }
  }
}
