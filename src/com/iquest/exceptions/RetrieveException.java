package com.iquest.exceptions;

/**
 * Exception used when something goes wrong retrieving the players.
 *
 * @author Laura Mamina
 */
public class RetrieveException extends RuntimeException {
  public RetrieveException(String message, Exception exception) {
	super(message, exception);
  }

  public RetrieveException(String message) {
	super(message);
  }
}
