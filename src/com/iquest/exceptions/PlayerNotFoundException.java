package com.iquest.exceptions;

/**
 * Thrown when a user who is not in the game tries to mark.
 *
 * @author Laura Mamina
 */
public class PlayerNotFoundException extends RuntimeException {

  private String userMailAddress;

  public PlayerNotFoundException(String userMailAddress) {
    super();
    this.userMailAddress = userMailAddress;
  }

  public String getUserMailAddress() {
    return userMailAddress;
  }
}
