package com.iquest.exceptions;

/**
 * Thrown when a user tries to mark in the game, but it is not his turn.
 *
 * @author Laura Mamina
 */
public class ShiftException extends RuntimeException {

  public ShiftException() {
    super();
  }
}
