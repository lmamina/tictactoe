package com.iquest.model;

/**
 * @author Laura Mamina
 */
public enum GameStatus {
  STATUS_WAITING_FOR_PLAYER {
    @Override
    public String getStatus() {
      return "Waiting for player";
    }
  },

  STATUS_IN_PROGRESS {
    @Override
    public String getStatus() {
      return "In progress";
    }
  },

  STATUS_END {
    @Override
    public String getStatus() {
      return "Ended";
    }
  };

  public abstract String getStatus();
}
