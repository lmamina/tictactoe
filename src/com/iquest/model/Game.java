package com.iquest.model;

import com.iquest.exceptions.PlayerNotFoundException;
import com.iquest.exceptions.ShiftException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Game {

  private String name;
  private int matrixSize;
  private String[][] matrix;
  private Player player1;
  private Player player2;
  private GameStatus gameStatus;

  private CharacterOption player1Option;
  private CharacterOption player2Option;
  private static final String FREE_CELL = " ";

  private boolean firstPlayerTurn = false;
  private GameEnded gameEnded;
  private static final Logger LOGGER = LoggerFactory.getLogger(Game.class);

  public Game(String name, int matrixSize) {
    this.name = name;
    gameStatus = GameStatus.STATUS_WAITING_FOR_PLAYER;
    setMatrixSize(matrixSize);
    gameEnded = new GameEnded(matrixSize);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CharacterOption getPlayer1Option() {
    return player1Option;
  }

  public void setPlayer1Option(CharacterOption option) {
    this.player1Option = option;
  }

  public CharacterOption getPlayer2Option() {
    return player2Option;
  }

  public void setPlayer2Option(CharacterOption player2Option) {
    this.player2Option = player2Option;
  }

  public Player getPlayer1() {
    return player1;
  }

  public void setPlayer1(Player player1) {
    this.player1 = player1;
  }

  public Player getPlayer2() {
    return player2;
  }

  public void setPlayer2(Player player2) {
    this.player2 = player2;
  }

  public boolean isEnded() {
    return gameStatus.equals(GameStatus.STATUS_END);
  }

  public String[][] getMatrix() {
    return matrix;
  }

  public void setMatrixSize(int matrixSize) {
    this.matrixSize = matrixSize;
    matrix = new String[matrixSize][matrixSize];

    for (int i = 0; i < matrixSize; i++) {
      for (int j = 0; j < matrixSize; j++) {
        matrix[i][j] = FREE_CELL;
      }
    }
  }

  public void setGameStatus(GameStatus gameStatus) {
    this.gameStatus = gameStatus;
  }

  public GameStatus getGameStatus() {
    return gameStatus;
  }

  public boolean isFirstPlayerTurn() {
    return firstPlayerTurn;
  }

  public Player getWinner() {
    return gameEnded.getWinner();
  }

  public GameEnded.MatrixPosition getWonPosition() {
    return gameEnded.getPosition();
  }

  public int getWonIndexForPosition() {
    return gameEnded.getIndex();
  }

  public int getWinningPoints() {
    return gameEnded.getPoints();
  }

  public void mark(Player currentPlayer, int row, int column) {
    LOGGER.debug("User: " + currentPlayer.getUsername() + " tries to mark row: " + row + " column: " + column);

    //Nothing happens if the game ended.
    if (gameStatus.equals(GameStatus.STATUS_END)) {
      return;
    }

    //Try to get the character option for marking
    CharacterOption characterOption;
    if (currentPlayer.equals(player1)) {
      if (!isFirstPlayerTurn()) {
        throw new ShiftException();
      }
      characterOption = player1Option;
    }
    else if (currentPlayer.equals(player2)) {
      if (isFirstPlayerTurn()) {
        throw new ShiftException();
      }
      characterOption = player2Option;
    }
    else {
      throw new PlayerNotFoundException("Player not in the game");
    }

    //Mark
    matrix[row][column] = characterOption.toString();
    boolean gameEnded = checkGameEnded(this.gameEnded);
    if (gameEnded) {
      gameStatus = GameStatus.STATUS_END;
      this.gameEnded.setWinner(firstPlayerTurn ? player1 : player2);
    }
    else {
      boolean matrixFull = checkMatrixFull();
      if (matrixFull) {
        gameStatus = GameStatus.STATUS_END;
      }
    }
    changePlayerShift();
  }

  private boolean checkMatrixFull() {
    for (int line=0; line<matrixSize;line++) {
      for (int column=0; column<matrixSize; column++) {
        if (matrix[line][column] == FREE_CELL) {
          return false;
        }
      }
    }
    return true;
  }

  private boolean checkGameEnded(GameEnded gameEnded) {
    int index = GameEndedUtils.verifyGameEndedForLines(matrix, matrixSize, FREE_CELL);
    if (index != -1) {
      gameEnded.setIndex(index);
      gameEnded.setPosition(GameEnded.MatrixPosition.ROW);
      return true;
    }

    index = GameEndedUtils.verifyGameEndedForColumns(matrix, matrixSize, FREE_CELL);
    if (index != -1) {
      gameEnded.setIndex(index);
      gameEnded.setPosition(GameEnded.MatrixPosition.COLUMN);
      return true;
    }

    boolean ended = GameEndedUtils.verifyGameEndedSecondaryDiagonal(matrix, matrixSize, FREE_CELL);
    if (ended) {
      gameEnded.setPosition(GameEnded.MatrixPosition.SECONDARY_DIAGONAL);
      return true;
    }

    ended = GameEndedUtils.verifyGameEndedMainDiagonal(matrix, matrixSize, FREE_CELL);
    if (ended) {
      gameEnded.setPosition(GameEnded.MatrixPosition.MAIN_DIAGONAL);
      return true;
    }

    return false;
  }



  private void changePlayerShift() {
    firstPlayerTurn = firstPlayerTurn == true ? false : true;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Game)) {
      return false;
    }

    if (name.equals(((Game) o).getName())) {
      return true;
    }

    return false;
  }
}