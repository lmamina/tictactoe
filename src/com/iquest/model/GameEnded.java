package com.iquest.model;

/**
 * Class that stores details when the game is ended.
 *
 * @author Laura Mamina
 */
public class GameEnded {
  private Player winner;
  private MatrixPosition position;
  private int index;
  private int points;

  public GameEnded(int points) {
    this.points = points;
  }

  public Player getWinner() {
    return winner;
  }

  public void setWinner(Player winner) {
    this.winner = winner;
    winner.addPoints(points);
  }

  public MatrixPosition getPosition() {
    return position;
  }

  public void setPosition(MatrixPosition position) {
    this.position = position;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public int getPoints() {
    return points;
  }

  public enum MatrixPosition {
    ROW, COLUMN, MAIN_DIAGONAL, SECONDARY_DIAGONAL;
  }
}
