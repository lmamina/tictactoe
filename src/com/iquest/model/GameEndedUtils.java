package com.iquest.model;

/**
 * @author Laura Mamina
 */
public class GameEndedUtils {

  public static int verifyGameEndedForLines(String[][] matrix, int matrixSize, String freeCell) {
    for (int i = 0; i < matrixSize; i++) {
      int j = 0;
      String c = matrix[i][j];

      if (c.equals(freeCell)) {
        continue;
      }

      for (j = 1; j < matrixSize; j++) {
        if (!c.equals(matrix[i][j])) {
          break;
        }
      }
      if (j == matrixSize) {
        return i;
      }
    }
    return -1;
  }


  public static int verifyGameEndedForColumns(String[][] matrix, int matrixSize, String freeCell) {
    for (int j = 0; j < matrixSize; j++) {

      int i = 0;
      String c = matrix[i][j];

      if (c.equals(freeCell)) {
        continue;
      }

      for (i = 1; i < matrixSize; i++) {
        if (!c.equals(matrix[i][j])) {
          break;
        }
      }

      if (i == matrixSize) {
        return j;
      }
    }
    return -1;
  }


  public static boolean verifyGameEndedSecondaryDiagonal(String[][] matrix, int matrixSize, String freeCell) {
    for (int i = 0; i < matrixSize; i++) {
      int j = matrixSize - 1;
      String c = matrix[i][j];

      if (c.equals(freeCell)) {
        continue;
      }

      for (j = matrixSize - 2; j >= 0; j--) {
        if (!c.equals(matrix[i][j])) {
          break;
        }
      }

      if (j == -1) {
        return true;
      }
    }
    return false;
  }

  public static boolean verifyGameEndedMainDiagonal(String[][] matrix, int matrixSize, String freeCell) {
    int i = 0;
    String c = matrix[i][i];

    if (!c.equals(freeCell)) {

      for (i = 1; i < matrixSize; i++) {
        if (!c.equals(matrix[i][i])) {
          break;
        }
      }
    }
    if (i == matrixSize) {
      return true;
    }
    return false;
  }
}
