package com.iquest.model;

/**
 * Enumerates the options a player has when marking in a game.
 */
public enum CharacterOption {
  X, O;
}
