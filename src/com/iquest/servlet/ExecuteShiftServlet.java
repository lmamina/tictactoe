package com.iquest.servlet;

import com.iquest.exceptions.ShiftException;
import com.iquest.model.Game;
import com.iquest.model.Player;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Executes a shift; get the game and gives him the row and column to mark the move.
 *
 * @author laura.mamina
 */
public class ExecuteShiftServlet extends HttpServlet {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExecuteShiftServlet.class);
  private static final long serialVersionUID = 1L;

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    LOGGER.debug("Start executing ExecuteShiftServlet");

    int row = Integer.parseInt(request.getParameter("row"));
    int column = Integer.parseInt(request.getParameter("column"));
    String gameName = request.getParameter("gameName");

    GameRepository gameRepository = (GameRepository) getServletContext().getAttribute(GameRepository.class.getName());
    Game game = gameRepository.get(gameName);

    Player currentPlayer = (Player) request.getSession().getAttribute("player");

    // if the current player is trying to access another game he is not involved
    // in, he's redirected to the page with the game list
    if (!(game.getPlayer1().equals(currentPlayer) || game.getPlayer2().equals(currentPlayer))) {
      response.sendRedirect("secure/games.jsp");
    }

    try {
      game.mark(currentPlayer, row, column);
    } catch (ShiftException e) {
      // TODO: do/show something when the user try to do a move when it's not his turn;
    }

    request.getRequestDispatcher("/secure/refreshTable?gameName=" + gameName).forward(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    doGet(request, response);
  }

}
