package com.iquest.servlet;

import com.iquest.model.Game;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Creates a game and add it to the repository. Then redirects for storing the player options.
 *
 * @author Laura Mamina
 */
public class CreateNewGameServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LoggerFactory.getLogger(CreateNewGameServlet.class);

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    LOGGER.debug("Executing CreateNewGameServlet");

    String gameName = request.getParameter("name");
    int matrixSize = Integer.parseInt(request.getParameter("matrixSize"));
    Game game = new Game(gameName, matrixSize);

    GameRepository gameRepository = (GameRepository) getServletContext().getAttribute(GameRepository.class.getName());
    gameRepository.add(game);

    String option = request.getParameter("option");

    response.sendRedirect(getServletContext().getContextPath() + "/secure/playOptions?gameName=" + game.getName() + "&option=" + option);
  }
}
