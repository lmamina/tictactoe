package com.iquest.servlet;

import com.iquest.model.Game;
import com.iquest.model.GameStatus;
import com.iquest.model.GameEnded;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RefreshTableServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LoggerFactory.getLogger(RefreshTableServlet.class);

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    LOGGER.debug("Start executing RefreshTableServlet");
    String gameName = request.getParameter("gameName");

    GameRepository gameRepository = (GameRepository) getServletContext().getAttribute(GameRepository.class.getName());
    Game game = gameRepository.get(gameName);

    request.setAttribute("matrix", game.getMatrix());

    if (game.isEnded()) {
      request.setAttribute("gameEnded", true);
      request.setAttribute("winner", game.getWinner());
      request.setAttribute("position", game.getWonPosition());
      if (game.getWonPosition() == GameEnded.MatrixPosition.ROW || game.getWonPosition() == GameEnded.MatrixPosition.COLUMN) {
        request.setAttribute("index", game.getWonIndexForPosition());
      }
    }
    else {
      if (game.getGameStatus().equals(GameStatus.STATUS_WAITING_FOR_PLAYER)) {
        request.setAttribute("userShift", "another player to join");
      }
      else {
        request.setAttribute("userShift", game.isFirstPlayerTurn() ? game.getPlayer1().getUsername() : game.getPlayer2().getUsername());
      }
    }

    request.getRequestDispatcher("/secure/gameTable.jsp").forward(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    doGet(request, response);
  }

}
