package com.iquest.servlet;

import com.iquest.model.Game;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Get the game with the name given in request and sets its matrix as an attribute. Dispatches to a page for playing game.
 *
 * @author Laura Mamina
 */
public class PlayServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LoggerFactory.getLogger(PlayServlet.class);

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    LOGGER.debug("Start executing PlayServlet");

    GameRepository gameRepository = (GameRepository) getServletContext().getAttribute(GameRepository.class.getName());
    Game game = gameRepository.get(request.getParameter("gameName"));

    request.setAttribute("matrix", game.getMatrix());
    request.getRequestDispatcher("/secure/play.jsp?gameName=" + game.getName()).forward(request, response);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
  }
}
