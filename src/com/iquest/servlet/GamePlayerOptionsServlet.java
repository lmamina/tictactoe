package com.iquest.servlet;

import com.iquest.model.CharacterOption;
import com.iquest.model.Game;
import com.iquest.model.GameStatus;
import com.iquest.model.Player;
import com.iquest.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Get the players' options for the game.
 *
 * @author laura.mamina
 */
public class GamePlayerOptionsServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  private static final Logger LOGGER = LoggerFactory.getLogger(GamePlayerOptionsServlet.class);

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    //Get the game
    GameRepository gameRepository = (GameRepository) getServletContext().getAttribute(GameRepository.class.getName());
    Game game = gameRepository.get(request.getParameter("gameName"));

    //Assign the player to the game.
    Player currentPlayer = (Player) request.getSession().getAttribute("player");
    if (game.getPlayer1() == null) {
      game.setPlayer1(currentPlayer);

      if (game.getPlayer1Option() == null) {
        game.setPlayer1Option(request.getParameter("option").equals("X") ? CharacterOption.X : CharacterOption.O);
      }

      LOGGER.info("Game: " + game.getName() + " Player1: " + game.getPlayer1().getUsername() + " Option: " + game.getPlayer1Option());
    }
    else {
      game.setPlayer2(currentPlayer);
      game.setPlayer2Option(game.getPlayer1Option().equals(CharacterOption.X) ? CharacterOption.O : CharacterOption.X);
      game.setGameStatus(GameStatus.STATUS_IN_PROGRESS);

      LOGGER.info("Game: " + game.getName() + " Player2: " + game.getPlayer2().getUsername() + " Option: " + game.getPlayer2Option());
    }

    response.sendRedirect(getServletContext().getContextPath() + "/secure/play?gameName=" + request.getParameter("gameName"));
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

}
