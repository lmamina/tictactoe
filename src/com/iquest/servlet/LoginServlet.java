package com.iquest.servlet;

import com.iquest.model.Player;
import com.iquest.repository.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Gets the username and the password and puts on the session the player if anyone exists. Redirects to the page with the list of games if the authentication succeed or to the login page if it
 * doesn't.
 *
 * @author Laura Mamina
 */
public class LoginServlet extends HttpServlet {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
  private static final long serialVersionUID = 1L;

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    LOGGER.trace("Response: " + response.getCharacterEncoding());
    LOGGER.trace("Request: " + request.getCharacterEncoding());

    String username = request.getParameter("username");
    String password = request.getParameter("password");

    PlayerRepository playerRepository = (PlayerRepository) getServletContext().getAttribute(PlayerRepository.class.getName());
    Player player = playerRepository.get(username, password);

    // if the login data is correct, it shows a page with games; else it goes to the login page
    if (player != null) {
      LOGGER.debug("Login successful for the user having the username: " + username + " and the password: " + password);
      request.getSession().setAttribute("player", player);
      response.sendRedirect(getServletContext().getContextPath() + "/secure/games.jsp");
    }
    else {
      LOGGER.debug("Login failed for the user having the username: " + username + " and the password: " + password);
      request.getSession().setAttribute("loginFailed", true);
      response.sendRedirect(getServletContext().getContextPath() + "/login.jsp");
    }
  }
}
