package com.iquest.servlet;

import com.iquest.config.LanguageMessages;
import com.iquest.config.Translations;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.iquest.config.ApplicationConfig.DEFAULT_LANGUAGE;
import static com.iquest.config.ApplicationConfig.RESOURCE_BUNDLE_BEAN;
import static com.iquest.config.ApplicationConfig.TRANSLATIONS_BEAN;

/**
 * @author Laura Mamina
 */
public class LocalizationServlet extends HttpServlet {

  private static final String LOGIN_PAGE = "/login.jsp";

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Translations translations = (Translations) getServletContext().getAttribute(TRANSLATIONS_BEAN.value());

    String language = req.getParameter("language");
    Locale locale = language == null ? translations.getDefaultLocale() : new Locale(language);

    ResourceBundle resourceBundle = translations.getBundle(locale);
    LanguageMessages languageMessages = new LanguageMessages();
    languageMessages.setBundle(resourceBundle);
    req.getSession().setAttribute(RESOURCE_BUNDLE_BEAN.value(), languageMessages);

    resp.sendRedirect(getServletContext().getContextPath() + LOGIN_PAGE);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }
}
